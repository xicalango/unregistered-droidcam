#!/usr/bin/env python3

from dataclasses import dataclass
from enum import Enum

class CoinType(Enum):
    MY = 1
    ES_ZETT = 2

@dataclass
class Coin:
    value: int
    type: CoinType

class My(Coin):
    def __init__(self, value: int):
        super().__init__(value, CoinType.MY) 

class EsZett(Coin):
    def __init__(self, value: int):
        super().__init__(value, CoinType.ES_ZETT) 

class Droid:
    def perform_trick(self, coin: Coin):
        pass

class BB8(Droid):
    def perform_trick(self, coin: Coin):
        if coin.type == CoinType.MY and coin.value == 1:
            print("BB8 uses jetpack. yeee")
        else:
            print("nonono")

class D2R2(Droid):
    def perform_trick(self, coin: Coin):
        if coin.type == CoinType.ES_ZETT and coin.value == 7:
            print("scramble dat egg")
        else:
            print("nonono")


droid1: Droid = BB8()
droid2: Droid = D2R2()

droid1.perform_trick(My(1))
droid1.perform_trick(EsZett(1))

droid2.perform_trick(My(7))
droid2.perform_trick(EsZett(7))

