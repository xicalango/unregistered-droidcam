#!/usr/bin/env python3

from dataclasses import dataclass
from enum import Enum

@dataclass
class Coin:
    value: int

    def dispatch(self, droid: 'Droid'):
        pass

class My(Coin):
    def dispatch(self, droid: 'Droid'):
        droid.perform_trick_my(self)

class EsZett(Coin):
    def dispatch(self, droid: 'Droid'):
        droid.perform_trick_es_zett(self)

class Droid:
    def perform_trick(self, coin: Coin):
        coin.dispatch(self)

    def perform_trick_my(self, coin: My):
        print("nonono")

    def perform_trick_es_zett(self, coin: EsZett):
        print("nonono")

class BB8(Droid):
    def perform_trick_my(self, coin: My):
        if coin.value == 1:
            print("BB8 uses jetpack. yeee")
        else:
            print("value no")

class D2R2(Droid):
    def perform_trick_es_zett(self, coin: EsZett):
        if coin.value == 7:
            print("scramble dat egg")
        else:
            print("value no")


droid1: Droid = BB8()
droid2: Droid = D2R2()

droid1.perform_trick(My(1))
droid1.perform_trick(EsZett(1))

droid2.perform_trick(My(7))
droid2.perform_trick(EsZett(7))

