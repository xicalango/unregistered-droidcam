#!/usr/bin/env python3

from dataclasses import dataclass

@dataclass
class Coin:
    value: int

class My(Coin):
    pass

class EsZett(Coin):
    pass

class Droid:
    def perform_trick(self, coin: Coin):
        pass

class BB8(Droid):
    def perform_trick(self, coin: Coin):
        if isinstance(coin, My) and coin.value == 1:
            print("BB8 uses jetpack. yeee")
        else:
            print("nonono")

class D2R2(Droid):
    def perform_trick(self, coin: Coin):
        if isinstance(coin, EsZett) and coin.value == 7:
            print("scramble dat egg")
        else:
            print("nonono")


droid1: Droid = BB8()
droid2: Droid = D2R2()

droid1.perform_trick(My(1))
droid1.perform_trick(EsZett(1))

droid2.perform_trick(My(7))
droid2.perform_trick(EsZett(7))

