
#[derive(Debug)]
enum Coin {
    My(i64),
    EsZett(i64),
}

trait Trickster {
    fn do_trick(&self, coin: Coin);
}

struct BB8;
struct D2R2;

impl Trickster for BB8 {
    fn do_trick(&self, coin: Coin) {
        match coin {
            Coin::My(1) => println!("backflip"),
            _ => println!("invalid input"),
        }
    }
}

impl Trickster for D2R2 {
    fn do_trick(&self, coin: Coin) {
        match coin {
            Coin::EsZett(7) => println!("egg"),
            _ => println!("invalid input"),
        }
    }
}

fn do_trick(trickster: &impl Trickster, coin: Coin) {
    trickster.do_trick(coin)
}

fn main() {
    let bb8 = BB8;
    let d2r2 = D2R2;

    do_trick(&bb8, Coin::My(1));
    do_trick(&bb8, Coin::EsZett(7));

    do_trick(&d2r2, Coin::My(1));
    do_trick(&d2r2, Coin::EsZett(7));
}
